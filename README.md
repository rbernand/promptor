# README #

## Promptor - Python Library to create prompt loop via callback ##

* Usage
* Example

## Usage ##

```
import promptor
import sys

def echo (*args):
    print (args)

prompt = promptor.new()
prompt.addCommand("print", action=echo)
prompt.addCommand("exit", action=sys.exit)
#...
prompt.start()
```

example output: 
```
rbernand at e2r2p20 $> python3 t.c                                                                                                                                          (master)
$>print
()
$>print a b c 5
('a', 'b', 'c', '5')
$>prin
Unknow command: prin
	help () : Display this message.
	print () :
	exit () :
$>exit
```
## Minidocs ##

* new([text=prompt_str]) -> Prompor
    * text could be a string or a function

* Promptor.addCommand(name, action, [nbargs=0, transform=[]])
    * name: a string
    * action: a method, or function
    * nbargs: number of arguments required
    * transform: list of contructor, to map each arguement